package com.kembachhav.mealaround;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReviewRatingAdapter extends RecyclerView.Adapter<ReviewRatingAdapter.ViewHolder> {

    ArrayList<ReviewRatingModel> mArraylistReviewRating;
    Context context;


    public ReviewRatingAdapter(ArrayList<ReviewRatingModel> mArraylistReviewRating, Context context) {
        this.mArraylistReviewRating = mArraylistReviewRating;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.review_rating_recycler, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        ReviewRatingModel mainDetail_reviewRating_model = mArraylistReviewRating.get(position);
        viewHolder.txtName.setText(mainDetail_reviewRating_model.getName());

    }

    @Override
    public int getItemCount() {
        return mArraylistReviewRating.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtName)
        TextView txtName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);        }
    }
}
