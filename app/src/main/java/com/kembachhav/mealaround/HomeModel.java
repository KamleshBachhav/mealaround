package com.kembachhav.mealaround;

public class HomeModel {

    String name;
    String area;
    String number;

    public HomeModel(String name, String area,String number) {
        this.name = name;
        this.area = area;
        this.number = number;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getArea() { return area; }
    public void setArea(String area) { this.area = area; }

    public String getNumber() {
        return number;
    }
    public void setNumber(String number) {
        this.number = number;
    }
}
