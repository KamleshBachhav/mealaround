package com.kembachhav.mealaround;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentHome extends Fragment {

    RelativeLayout relativeLayout;
    @BindView(R.id.rv_contract)
    RecyclerView rvContract;
    private ArrayList<HomeModel> mArrayList = new ArrayList<>();
    Context context;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        relativeLayout = (RelativeLayout) inflater.inflate(R.layout.activity_fragment_home, container, false);
        ButterKnife.bind(this, relativeLayout);
        context = getActivity();
        rvContract.setLayoutManager(new GridLayoutManager(context,2));
        HomeAdapter adapter = new HomeAdapter(mArrayList, context);
        rvContract.setAdapter(adapter);
        getMyBookingData();
        return relativeLayout;
    }

    private void getMyBookingData() {
        HomeModel myHistoryModel = null;
        myHistoryModel = new HomeModel("Emma Thomas", "100 meter","3.5");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Robert Wilson","200 meter","4.5");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Emma Thomas","300 meter","5.0");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Robert Wilson","400 meter","3.0");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Emma Thomas","200 meter","4.5");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Robert Wilson","300 meter","4.0");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Emma Thomas","400 meter","5.0");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Robert Wilson","500 meter","3.5");
        mArrayList.add(myHistoryModel);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
