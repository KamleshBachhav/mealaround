package com.kembachhav.mealaround;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MessRegistration extends AppCompatActivity {

    Context context;
    FirebaseFirestore myDB;
    FirebaseAuth mAuth;
    @BindView(R.id.et_Lat)
    AppCompatEditText etLat;
    @BindView(R.id.et_Long)
    AppCompatEditText etLong;
    //uri to store file
    private Uri filePath;
    //firebase objects
    private StorageReference storageReference;
    @BindView(R.id.tv_meal)
    TextView tvMeal;
    @BindView(R.id.tv_around)
    TextView tvAround;
    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.tv_sign)
    TextView tvSign;
    @BindView(R.id.tv_in)
    TextView tvIn;
    @BindView(R.id.rl_signup)
    RelativeLayout rlSignup;
    @BindView(R.id.et_mName)
    AppCompatEditText etMName;
    @BindView(R.id.et_oName)
    AppCompatEditText etOName;
    @BindView(R.id.et_mNumber)
    AppCompatEditText etMNumber;
    @BindView(R.id.et_email)
    AppCompatEditText etEmail;
    @BindView(R.id.et_addr1)
    AppCompatEditText etAddr1;
    @BindView(R.id.et_landmark)
    AppCompatEditText etLandmark;
    @BindView(R.id.showImg)
    AppCompatImageView showImg;
    @BindView(R.id.uploadImg)
    AppCompatImageView uploadImg;
    @BindView(R.id.capture)
    TextView capture;
    @BindView(R.id.ct_banner)
    FrameLayout ctBanner;
    @BindView(R.id.showImg1)
    AppCompatImageView showImg1;
    @BindView(R.id.uploadImg1)
    AppCompatImageView uploadImg1;
    @BindView(R.id.capture1)
    TextView capture1;
    @BindView(R.id.ct_area)
    FrameLayout ctArea;
    @BindView(R.id.btn_signUp)
    Button btnSignUp;
    Geocoder geocoder;
    List<Address> addresses;
    double Longg,Latt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mess_registration);
        ButterKnife.bind(this);
        context = MessRegistration.this;
        // Init FireStore
        mAuth = FirebaseAuth.getInstance();
        myDB = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        Intent intent = getIntent();
        Longg = intent.getDoubleExtra("Long",0);
        Latt = intent.getDoubleExtra("Latt",0);
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(Latt, Longg, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }
        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
        etAddr1.setText(address);
       // Toast.makeText(MessRegistration.this, "Address"+address+"::"+city+state, Toast.LENGTH_SHORT).show();
    }

    @OnClick({R.id.ct_banner, R.id.ct_area, R.id.btn_signUp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ct_banner:
                selectImage();
                break;
            case R.id.ct_area:
                selectImage();
                break;
            case R.id.btn_signUp:
                Map<String, Object> data = new HashMap<>();
                data.put("Mess Name", etMName.getText().toString());
                data.put("Owner Name", etOName.getText().toString());
                data.put("Email", etEmail.getText().toString());
                data.put("Address", etAddr1.getText().toString());
                data.put("Number", etMNumber.getText().toString());
                data.put("Landmark", etLandmark.getText().toString());
                data.put("Latitude", etLat.getText().toString());
                data.put("Longitude", etLong.getText().toString());
                FirebaseUser user = mAuth.getCurrentUser();
                assert user != null;
                myDB.collection("Mess").document(user.getUid()).set(data)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                uploadFile();
                                Intent intent1 = new Intent(MessRegistration.this, AddMenuActivity.class);
                                startActivity(intent1);
                                Toast.makeText(MessRegistration.this, "Registration successful", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(MessRegistration.this, "Error while adding the data : " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
        }
    }


    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MessRegistration.this);
        builder.setTitle("Upload Photo!!!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take from Camera")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    try {
                        Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                        m.invoke(null);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    startActivityForResult(intent, 1);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {

            } else if (requestCode == 2 && data != null && data.getData() != null) {
                filePath = data.getData();
                try {
                    String[] file = {MediaStore.Images.Media.DATA};
                    Cursor c = getContentResolver().query(filePath, file, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(file[0]);
                    String picturePath = c.getString(columnIndex);
                    c.close();

                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                    uploadImg.setVisibility(View.GONE);
                    capture.setVisibility(View.GONE);
                    showImg.setVisibility(View.VISIBLE);
                    showImg.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void uploadFile() {
        FirebaseUser user = mAuth.getCurrentUser();
        //checking if file is available
        if (filePath != null) {
            //getting the storage reference
            StorageReference sRef = storageReference.child("Mess/" + user.getUid() + "/" + "MessBanner" + "." + getFileExtension(filePath));
            //adding the file to reference
            sRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //displaying success toast
                            Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();
                            //adding an upload to firebase database
                            assert user != null;
                            myDB.collection("Mess").document(user.getUid()).update("messBanner", taskSnapshot.getUploadSessionUri().toString());
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //displaying the upload progress
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else {
            //display an error if no file is selected
        }
    }
}
