package com.kembachhav.mealaround;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {
    private static final int REQUEST_CODE_SOME_FEATURES_PERMISSIONS = 04;

    //a constant for detecting the login intent result
    private static final int RC_SIGN_IN = 3;
    //Tag for the logs optional
    private static final String TAG = "Demo";
    //creating a GoogleSignInClient object
    GoogleSignInClient mGoogleSignInClient;
    //And also a Firebase Auth object
    FirebaseAuth mAuth;
    FirebaseFirestore myDB;
    String operator;
    private static final int PERMISSION_REQUEST_CODE = 200;

    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.tv_sign)
    TextView tvSign;
    @BindView(R.id.tv_in)
    TextView tvIn;
    @BindView(R.id.rl_signup)
    RelativeLayout rlSignup;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.btn_signIn)
    Button btnSignIn;
    FusedLocationProviderClient mFusedLocationClient;
    int PERMISSION_ID = 44;
    Location location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        myDB = FirebaseFirestore.getInstance();
        //first we intialized the FirebaseAuth object
        mAuth = FirebaseAuth.getInstance();
        //Then we need a GoogleSignInOptions object
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        //Then we will get the GoogleSignInClient object from GoogleSignIn class
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        //Now we will attach a click listener to the sign_in_button
        // and inside onClick() method we are calling the signIn() method that will open
        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        getLastLocation();
    }
    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        operator = intent.getStringExtra("Op");
        //Toast.makeText(LoginActivity.this, "onStart invoked"+operator, Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    Toast.makeText(LoginActivity.this, "Current Location"+location.getLatitude()+"::"+location.getLongitude(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                );
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }


    @SuppressLint("MissingPermission")
    private void requestNewLocationData() {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
           Toast.makeText(LoginActivity.this, ".Last Location"+mLastLocation.getLatitude()+"::"+mLastLocation.getLongitude(), Toast.LENGTH_SHORT).show();
        }
    };

    private boolean checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (checkPermissions()) {
            getLastLocation();
        }
    }

    //this method is called on click
    private void signIn() {
        //getting the google signin intent
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        //starting the activity for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //if the requestCode is the Google Sign In code that we defined at starting
        if (requestCode == RC_SIGN_IN) {
            //Getting the GoogleSignIn Task
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                //Google Sign In was successful, authenticate with Firebase, Returns the error information for the user
                GoogleSignInAccount account = task.getResult(ApiException.class);
                //authenticating with firebase
                assert account != null;
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "AuthWithGoogle:" + acct.getId());
        //getting the auth credential
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        //Now using firebase we are signing in the user here
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signIn:success");
                            // FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(LoginActivity.this, "User Signed In", Toast.LENGTH_SHORT).show();
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseFirestore myDB = FirebaseFirestore.getInstance();
                            FirebaseUser user = mAuth.getCurrentUser();
                            assert user != null;
                            DocumentReference docRef = myDB.collection("Users").document(user.getUid());
                            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        DocumentSnapshot document = task.getResult();
                                        assert document != null;
                                        if (document.exists()) {
                                            if(operator.equals("MessOwner")){
                                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                intent.putExtra("Op", operator);
                                                intent.putExtra("Long",location.getLongitude());
                                                intent.putExtra("Latt",location.getLatitude());
                                                startActivity(intent);
                                            }else{
                                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                intent.putExtra("Op", operator);
                                                intent.putExtra("Long",location.getLongitude());
                                                intent.putExtra("Latt",location.getLatitude());
                                                startActivity(intent);
                                            }
                                        } else {
                                            if(operator.equals("MessOwner")){
                                                Intent intent = new Intent(LoginActivity.this, MessRegistration.class);
                                                intent.putExtra("Op", operator);
                                                intent.putExtra("Long",location.getLongitude());
                                                intent.putExtra("Latt",location.getLatitude());
                                                startActivity(intent);
                                            }else{
                                                Intent intent = new Intent(LoginActivity.this, UserRegistration.class);
                                                intent.putExtra("Op", operator);
                                                intent.putExtra("Long",location.getLongitude());
                                                intent.putExtra("Latt",location.getLatitude());
                                                startActivity(intent);
                                            }
                                        }
                                    } else {
                                        Log.d("", "get failed with ", task.getException());
                                    }
                                }
                            });
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signIn:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @OnClick({ R.id.btn_signIn})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.btn_signIn) {
            String mobile = etEmail.getText().toString().trim();
            if (mobile.isEmpty() || mobile.length() < 10) {
                etEmail.setError("Enter a valid mobile");
                etEmail.requestFocus();
                return;
            }
            Intent intent = new Intent(LoginActivity.this, MobileOtpActivity.class);
            intent.putExtra("mobile", mobile);
            intent.putExtra("Op", operator);
            startActivity(intent);
        }
    }
}
