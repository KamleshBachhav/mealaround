package com.kembachhav.mealaround.ui.profile;

import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.kembachhav.mealaround.R;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment {

    @BindView(R.id.profileimage)
    CircleImageView profileimage;
    @BindView(R.id.add_profile)
    CircleImageView addProfile;
    @BindView(R.id.et_FirstName)
    MaterialEditText etFirstName;
    @BindView(R.id.et_LastName)
    MaterialEditText etLastName;
    @BindView(R.id.et_Email)
    MaterialEditText etEmail;
    @BindView(R.id.et_Mobile)
    MaterialEditText etMobile;
    @BindView(R.id.et_Addressone)
    MaterialEditText etAddressone;
    @BindView(R.id.Sp_City)
    MaterialEditText SpCity;
    @BindView(R.id.btn_ChangePasword)
    Button btnChangePasword;
    FirebaseFirestore myDB;
    FirebaseAuth mAuth;
    String user;
    @BindView(R.id.latitude)
    MaterialEditText latitude;
    @BindView(R.id.longitude)
    MaterialEditText longitude;
    //uri to store file
    private Uri filePath;
    //firebase objects
    private StorageReference storageReference;
    private ProfileViewModel profileViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        profileViewModel =
                ViewModelProviders.of(this).get(ProfileViewModel.class);
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this, root);

        myDB = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        user = FirebaseAuth.getInstance().getCurrentUser().getUid();
        ReadSingleContact();
        return root;
    }

    private void ReadSingleContact() {
        DocumentReference docRef = myDB.collection("Users").document(user);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot doc = task.getResult();
                    assert doc != null;
                    etFirstName.setText(Objects.requireNonNull(doc.get("First Name")).toString());
                    etLastName.setText(Objects.requireNonNull(doc.get("Last Name")).toString());
                    etMobile.setText(Objects.requireNonNull(doc.get("Number")).toString());
                    etEmail.setText(Objects.requireNonNull(doc.get("Email")).toString());
                    etAddressone.setText(Objects.requireNonNull(doc.get("Address")).toString());
                    longitude.setVisibility(View.GONE);
                    latitude.setVisibility(View.GONE);
                    SpCity.setVisibility(View.GONE);
                }
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                    }
                });
    }

    @OnClick({R.id.add_profile, R.id.btn_ChangePasword})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_profile:
                selectImage();
                break;
            case R.id.btn_ChangePasword:
                break;
        }
    }


    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Upload Photo!!!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take from Camera")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    try {
                        Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                        m.invoke(null);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    startActivityForResult(intent, 1);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {

            } else if (requestCode == 2 && data != null && data.getData() != null) {
                filePath = data.getData();
                try {
                    String[] file = {MediaStore.Images.Media.DATA};
                    Cursor c = getActivity().getContentResolver().query(filePath, file, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(file[0]);
                    String picturePath = c.getString(columnIndex);
                    c.close();

                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                    profileimage.setImageBitmap(bitmap);
                    selectImage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getFileExtension(Uri uri) {
        ContentResolver cR = getActivity().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void uploadFile() {
        FirebaseUser user = mAuth.getCurrentUser();
        //checking if file is available
        if (filePath != null) {
            //getting the storage reference
            StorageReference sRef = storageReference.child("Users/" + user.getUid() + "/" + "Profile" + "." + getFileExtension(filePath));
            //adding the file to reference
            sRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //displaying success toast
                            Toast.makeText(getContext(), "File Uploaded ", Toast.LENGTH_LONG).show();
                            //adding an upload to firebase database
                            assert user != null;
                            myDB.collection("Users").document(user.getUid()).update("profile", taskSnapshot.getUploadSessionUri().toString());
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            Toast.makeText(getContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //displaying the upload progress
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        }
                    });
        } else {
            //display an error if no file is selected
        }
    }
}