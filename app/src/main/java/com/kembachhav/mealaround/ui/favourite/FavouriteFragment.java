package com.kembachhav.mealaround.ui.favourite;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kembachhav.mealaround.HomeAdapter;
import com.kembachhav.mealaround.HomeModel;
import com.kembachhav.mealaround.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavouriteFragment extends Fragment {
    Context context;
    private ArrayList<HomeModel> mArrayList = new ArrayList<>();
    @BindView(R.id.rv_contract)
    RecyclerView rvContract;
    private FavouriteViewModel favouriteViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        favouriteViewModel = ViewModelProviders.of(this).get(FavouriteViewModel.class);
        View root = inflater.inflate(R.layout.fragment_slideshow, container, false);
        ButterKnife.bind(this, root);
        context = getActivity();
        rvContract.setLayoutManager(new GridLayoutManager(context, 2));
        HomeAdapter adapter = new HomeAdapter(mArrayList, context);
        rvContract.setAdapter(adapter);
        getMyBookingData();
        return root;
    }


    private void getMyBookingData() {
        HomeModel myHistoryModel = null;
        myHistoryModel = new HomeModel("Emma Thomas", "Baner", "3.5");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Robert Wilson", "Chinchwad", "4.5");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Emma Thomas", "Baner", "5.0");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Robert Wilson", "Chinchwad", "3.0");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Emma Thomas", "Chinchwad", "4.5");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Robert Wilson", "Baner", "4.0");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Emma Thomas", "Baner", "5.0");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Robert Wilson", "Baner", "3.5");
        mArrayList.add(myHistoryModel);
    }
}