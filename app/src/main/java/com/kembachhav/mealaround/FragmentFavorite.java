package com.kembachhav.mealaround;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentFavorite extends Fragment {
    Context context;
    LinearLayout linearLayout;
    @BindView(R.id.rv_contract)
    RecyclerView rvContract;
    private ArrayList<HomeModel> mArrayList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        linearLayout = (LinearLayout) inflater.inflate(R.layout.activity_fragment_favorite, container, false);
        ButterKnife.bind(this, linearLayout);
        context = getActivity();
        rvContract.setLayoutManager(new GridLayoutManager(context,2));
        HomeAdapter adapter = new HomeAdapter(mArrayList, context);
        rvContract.setAdapter(adapter);
        getMyBookingData();
        return linearLayout;
    }

    private void getMyBookingData() {
        HomeModel myHistoryModel = null;
        myHistoryModel = new HomeModel("Emma Thomas", "Baner","3.5");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Robert Wilson","Chinchwad","4.5");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Emma Thomas","Baner","5.0");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Robert Wilson","Chinchwad","3.0");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Emma Thomas","Chinchwad","4.5");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Robert Wilson","Baner","4.0");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Emma Thomas","Baner","5.0");
        mArrayList.add(myHistoryModel);

        myHistoryModel = new HomeModel("Robert Wilson","Baner","3.5");
        mArrayList.add(myHistoryModel);
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
