package com.kembachhav.mealaround;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatToggleButton;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShowMessActivity extends AppCompatActivity {

    @BindView(R.id.header_image)
    ImageView headerImage;
    @BindView(R.id.imgbackArrow)
    ImageView imgbackArrow;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.mess_name)
    TextView messName;
    @BindView(R.id.starimage)
    ImageView starimage;
    @BindView(R.id.tv_Ratingcount)
    TextView tvRatingcount;
    @BindView(R.id.tv_totalrating)
    TextView tvTotalrating;
    @BindView(R.id.llreview)
    RelativeLayout llreview;
    @BindView(R.id.relativetitle)
    RelativeLayout relativetitle;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.mapButton)
    Button mapButton;
    @BindView(R.id.callButton)
    Button callButton;
    @BindView(R.id.relativeButton)
    RelativeLayout relativeButton;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.addr)
    TextView addr;
    @BindView(R.id.about_mess)
    TextView aboutMess;
    @BindView(R.id.btnReviewRating)
    Button btnReviewRating;
    @BindView(R.id.rlReviewRating)
    RecyclerView rlReviewRating;
    @BindView(R.id.scroll)
    NestedScrollView scroll;
    private static final int REQUEST_PHONE_CALL = 1;
    @BindView(R.id.img_fav)
    AppCompatToggleButton imgFav;
    ArrayList<ReviewRatingModel> mArrayReviewRatingList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_mess);
        ButterKnife.bind(this);

        //Managing CollapsingToolbar Title
        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout)
                findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        imgFav.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            Toast.makeText(ShowMessActivity.this, "Add as a favourite!", Toast.LENGTH_SHORT).show();
                            imgFav.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.favorite));
                        } else {
                            Toast.makeText(ShowMessActivity.this, "Remove from favourite!", Toast.LENGTH_SHORT).show();
                            imgFav.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.favorite1));
                        }
                    }
                });
        //Attaching Review Rating Data to Recycler
        LinearLayoutManager layoutManager4 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rlReviewRating.setLayoutManager(layoutManager4);
        ReviewRatingAdapter mainDetailReviewRatingAdapter = new ReviewRatingAdapter(mArrayReviewRatingList, this);
        rlReviewRating.setAdapter(mainDetailReviewRatingAdapter);
        reviewRatingData();
    }

    //Review Rating Dialog Method
    private void getReviewRatingdialog() {
        LayoutInflater inflater = LayoutInflater.from(ShowMessActivity.this);
        final View dialog = inflater.inflate(R.layout.review_rating_dailog, null);

        final AlertDialog builder = new AlertDialog.Builder(ShowMessActivity.this).create();
        builder.setView(dialog);
        builder.setCancelable(false);
        Button changepas = dialog.findViewById(R.id.btnSubmit);
        ImageView ivClose = dialog.findViewById(R.id.iv_ReviewRatingClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
        changepas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });
        builder.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.mapButton, R.id.callButton, R.id.btnReviewRating, R.id.imgbackArrow})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mapButton:
                break;
            case R.id.callButton:
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:8055767640"));
                if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    Activity#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.
                    ActivityCompat.requestPermissions(ShowMessActivity.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                    return;
                } else {
                    startActivity(callIntent);
                }
                break;
            case R.id.btnReviewRating:
                getReviewRatingdialog();
                break;
            case R.id.imgbackArrow:
                finish();
                break;
        }
    }
    //Review Rating data method
    private void reviewRatingData() {
        ReviewRatingModel reviewRatingModel = null;
        reviewRatingModel = new ReviewRatingModel("Jack Sparrow");
        mArrayReviewRatingList.add(reviewRatingModel);
        reviewRatingModel = new ReviewRatingModel("Jack Sparrow");
        mArrayReviewRatingList.add(reviewRatingModel);
        reviewRatingModel = new ReviewRatingModel("Jack Sparrow");
        mArrayReviewRatingList.add(reviewRatingModel);
    }


}
