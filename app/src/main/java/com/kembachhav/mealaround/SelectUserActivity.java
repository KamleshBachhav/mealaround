package com.kembachhav.mealaround;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class SelectUserActivity extends AppCompatActivity {

    @BindView(R.id.imgTraveler)
    CircleImageView imgTraveler;
    @BindView(R.id.llmessowner)
    LinearLayout llmessowner;
    @BindView(R.id.imgOperator)
    CircleImageView imgOperator;
    @BindView(R.id.lluser)
    LinearLayout lluser;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_user);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //if the user is already signed in we will close this activity
        //and take the Google Map activity
        if (mAuth.getCurrentUser() != null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    @OnClick({R.id.llmessowner, R.id.lluser})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llmessowner:
                Intent intent = new Intent(SelectUserActivity.this, LoginActivity.class);
                intent.putExtra("Op", "MessOwner");
                startActivity(intent);
                break;
            case R.id.lluser:
                Intent intent1 = new Intent(SelectUserActivity.this, LoginActivity.class);
                intent1.putExtra("Op", "Users");
                startActivity(intent1);
                break;
        }
    }
}
