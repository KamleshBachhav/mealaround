package com.kembachhav.mealaround;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.Viewholder> {

    @BindView(R.id.img_mess)
    AppCompatImageView imgMess;
    @BindView(R.id.tv_Name)
    TextView tvName;
    @BindView(R.id.tv_rating)
    TextView tvRating;
    @BindView(R.id.img_star)
    AppCompatImageView imgStar;
    private ArrayList<HomeModel> values;
    Context context;

    public HomeAdapter(ArrayList<HomeModel> values, Context context) {
        this.values = values;
        this.context = context;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_home, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        HomeModel historyResp = values.get(position);
        holder.tvName.setText(historyResp.getName());
        holder.tvMeter.setText(historyResp.getArea());
        holder.tvRating.setText(historyResp.getNumber());

        holder.imgMess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ShowMessActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    class Viewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_Name)
        TextView tvName;
        @BindView(R.id.img_mess)
        AppCompatImageView imgMess;
        @BindView(R.id.tv_meter)
        TextView tvMeter;
        @BindView(R.id.tv_rating)
        TextView tvRating;
        @BindView(R.id.img_star)
        AppCompatImageView imgStar;

        Viewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
