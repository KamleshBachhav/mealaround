package com.kembachhav.mealaround;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddMenuActivity extends AppCompatActivity {
    Context context;
    FirebaseFirestore myDB;
    FirebaseAuth mAuth;
    @BindView(R.id.tv_sign)
    TextView tvSign;
    @BindView(R.id.tv_in)
    TextView tvIn;
    @BindView(R.id.rl_signup)
    RelativeLayout rlSignup;
    @BindView(R.id.et_sb1)
    AppCompatEditText etSb1;
    @BindView(R.id.input_layout_name)
    TextInputLayout inputLayoutName;
    @BindView(R.id.et_sb2)
    AppCompatEditText etSb2;
    @BindView(R.id.et_rice)
    AppCompatEditText etRice;
    @BindView(R.id.et_dal)
    AppCompatEditText etDal;
    @BindView(R.id.et_roti)
    AppCompatEditText etRoti;
    @BindView(R.id.et_salad)
    AppCompatEditText etSalad;
    @BindView(R.id.et_sweet)
    AppCompatEditText etSweet;
    @BindView(R.id.btn_veg)
    Button btnVeg;
    @BindView(R.id.rl)
    RelativeLayout rl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_menu);
        ButterKnife.bind(this);
        context = AddMenuActivity.this;
        mAuth = FirebaseAuth.getInstance();
        myDB = FirebaseFirestore.getInstance();
    }

    @OnClick({R.id.btn_veg})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_veg:
                saveVegMenu();
                break;

        }
    }

    private void saveVegMenu(){
        Map<String, Object> data = new HashMap<>();
        data.put("Sabji1", etSb1.getText().toString());
        data.put("Sabji2 Name", etSb2.getText().toString());
        data.put("Rice", etRice.getText().toString());
        data.put("Salad", etSalad.getText().toString());
        data.put("Dal", etDal.getText().toString());
        data.put("Sweet", etSweet.getText().toString());
        data.put("Roti", etRoti.getText().toString());
        FirebaseUser user = mAuth.getCurrentUser();
        assert user != null;
        myDB.collection("MessMenu").document(user.getUid()).set(data)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        btnVeg.setText("Update");
                        etSb1.setEnabled(false);
                        etSb2.setEnabled(false);
                        etRice.setEnabled(false);
                        etSalad.setEnabled(false);
                        etDal.setEnabled(false);
                        etSweet.setEnabled(false);
                        etRoti.setEnabled(false);
                        Toast.makeText(AddMenuActivity.this, "Veg Menu Added successful", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(AddMenuActivity.this, "Error while adding the Menu : " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

}
