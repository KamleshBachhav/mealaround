package com.kembachhav.mealaround;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MobileOtpActivity extends AppCompatActivity {

    @BindView(R.id.tv_verify)
    TextView tvVerify;
    @BindView(R.id.btn_confirm)
    Button btnConfirm;
    @BindView(R.id.progressbar)
    ProgressBar progressbar;
    @BindView(R.id.editTextCode)
    EditText editTextCode;

    String operator;
    //These are the objects needed
    //It is the verification id that will be sent to the user
    private String mVerificationId;
    //firebase auth object
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_otp);
        ButterKnife.bind(this);
        //initializing objects
        mAuth = FirebaseAuth.getInstance();
        //getting mobile number from the previous activity
        //and sending the verification code to the number
        Intent intent = getIntent();
        String mobile = intent.getStringExtra("mobile");
        operator = intent.getStringExtra("Op");
        sendVerificationCode(mobile);
    }

    @OnClick(R.id.btn_confirm)
    public void onViewClicked() {
        String code = editTextCode.getText().toString().trim();
        if (code.isEmpty() || code.length() < 6) {
            editTextCode.setError("Enter valid code");
            editTextCode.requestFocus();
        }else{
            //verifying the code entered manually
            verifyVerificationCode(code);
        }
    }

    //the method is sending verification code
    //the country id is concatenated
    //you can take the country id as user input as well
    private void sendVerificationCode(String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+91" + mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }
    //the callback to detect the verification status
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                editTextCode.setText(code);
                //verifying the code
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(MobileOtpActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            mVerificationId = s;
        }
    };

    private void verifyVerificationCode(String code) {
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
        //signing the user
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(MobileOtpActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseFirestore myDB = FirebaseFirestore.getInstance();
                            FirebaseUser user = mAuth.getCurrentUser();
                            assert user != null;
                            DocumentReference docRef = myDB.collection("Users").document(user.getUid());
                            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        DocumentSnapshot document = task.getResult();
                                        assert document != null;
                                        if (document.exists()) {
                                            if(operator.equals("MessOwner")){
                                                Intent intent = new Intent(MobileOtpActivity.this, MainActivity.class);
                                                intent.putExtra("Op", operator);
                                                startActivity(intent);
                                            }else{
                                                Intent intent = new Intent(MobileOtpActivity.this, MainActivity.class);
                                                intent.putExtra("Op", operator);
                                                startActivity(intent);
                                            }
                                        } else {
                                            if(operator.equals("MessOwner")){
                                                Intent intent = new Intent(MobileOtpActivity.this, MessRegistration.class);
                                                intent.putExtra("Op", operator);
                                                startActivity(intent);
                                            }else{
                                                Intent intent = new Intent(MobileOtpActivity.this, UserRegistration.class);
                                                intent.putExtra("Op", operator);
                                                startActivity(intent);
                                            }
                                        }
                                    } else {
                                        Log.d("", "get failed with ", task.getException());
                                    }
                                }
                            });

                        } else {
                            //verification unsuccessful.. display an error message
                            String message = "Somthing is wrong, we will fix it soon...";
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            }
                            Snackbar snackbar = Snackbar.make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
                            snackbar.setAction("Dismiss", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });
                            snackbar.show();
                        }
                    }
                });
    }
}
