package com.kembachhav.mealaround;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class UserRegistration extends AppCompatActivity {

    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.tv_sign)
    TextView tvSign;
    @BindView(R.id.tv_in)
    TextView tvIn;
    @BindView(R.id.rl_signup)
    RelativeLayout rlSignup;
    @BindView(R.id.et_fname)
    EditText etFname;
    @BindView(R.id.et_lname)
    EditText etLname;
    @BindView(R.id.et_number)
    EditText etNumber;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_addr)
    EditText etAddr;
    @BindView(R.id.btn_signUp)
    Button btnSignUp;
    FirebaseFirestore myDB;
    FirebaseAuth mAuth;
    Geocoder geocoder;
    List<Address> addresses;
    double Longg,Latt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration);
        ButterKnife.bind(this);
        // Init FireStore
        mAuth = FirebaseAuth.getInstance();
        myDB = FirebaseFirestore.getInstance();
        Intent intent = getIntent();
        Longg = intent.getDoubleExtra("Long",0);
        Latt = intent.getDoubleExtra("Latt",0);
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(Latt, Longg, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }
        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
        etAddr.setText(address);
        // Toast.makeText(MessRegistration.this, "Address"+address+"::"+city+state, Toast.LENGTH_SHORT).show();
    }

    @OnClick({R.id.btn_signUp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_signUp:
                Map<String, Object> data = new HashMap<>();
               data.put("First Name", etFname.getText().toString());
                data.put("Last Name", etLname.getText().toString());
                data.put("Email", etEmail.getText().toString());
                data.put("Address", etAddr.getText().toString());
                data.put("Number", etNumber.getText().toString());
                FirebaseUser user = mAuth.getCurrentUser();
                assert user != null;
                myDB.collection("Users").document(user.getUid()).set(data)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                Intent intent1 =new Intent(UserRegistration.this,MainActivity.class);
                                startActivity(intent1);
                                Toast.makeText(UserRegistration.this, "Data added successfully", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(UserRegistration.this, "Error while adding the data : " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                break;
        }
    }
}
