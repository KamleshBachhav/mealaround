package com.kembachhav.mealaround;

public class ReviewRatingModel {
    String name;

    public ReviewRatingModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
